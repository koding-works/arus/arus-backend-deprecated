<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCounter extends Model
{
    use SoftDeletes;

    public function counter()
    {
        return $this->belongsTo('App\Counter');
    }
}
