<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Counter;
use \App\UserCounter;

class CounterController extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ], $code);
    }

    public function index()
    {
        try {
            $counter = Counter::get();
            $i= 0;
            $data=[];
            foreach($counter as $row){
                $data[$i++] = [
                    'counter' => $row,
                    'last_queue' => UserCounter::where('counter_id',$row->id)->where('queue_date',Date('Y-m-d'))->orderBy('queue_number','desc')->select('queue_number')->limit(1)->pluck('queue_number')->first(),
                    'current_queue' => UserCounter::where('counter_id',$row->id)->where('queue_date',Date('Y-m-d'))->where('is_processed','process')->select('queue_number')->limit(1)->pluck('queue_number')->first(),
                    'today' => Date('Y-m-d')
                ];
            }
            $message = "success";
            $row = $data;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function show($id){
        
        
    }

    public function store(Request $request){
        try {
            $counter = new Counter;
            $counter->name = $request->get('name');
            $counter->current_queue = 
            $counter->save();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function update(Request $request, $id){
        try {
            $counter = Counter::find($id);
            $counter->name = $request->get('name');
            $counter->save();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function destroy($id){
        try {
            $counter = Counter::find($id);
            $counter->delete();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }
}
