<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Doctor;
use \App\DoctorCategory;
use \App\User;
use \DB;

class DoctorController extends Controller
{
    /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $data['doctors'] = Doctor::get();
            return view('doctors.index')->with($data);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = DoctorCategory::orderBy('name','asc')->get();
        return view('doctors.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();
            $user->attachRole(3);

            $doctor = new Doctor;
            $doctor->user_id = $user->id;
            $doctor->doctor_category_id = $request->doctor_category_id;
            $doctor->save();

            DB::commit();
            return redirect('/doctors');
        } catch (Exception $err) {
            DB::rollBack();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['doctor'] = Doctor::find($id);
        $data['categories'] = DoctorCategory::orderBy('name','asc')->get();
        return view('doctors.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $doctor = Doctor::find($id);
            $doctor->doctor_category_id = $request->doctor_category_id;
            $doctor->save();
            
            $user = User::find($doctor->user_id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();

            DB::commit();
            return redirect('/doctors');
        } catch (Exception $err) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = Doctor::find($id);
        $doctor->delete();
    }
}
