<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\UserFamilyMember as Family;
use \App\User;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
        $data['families'] = Family::get();
        $data['user'] = User::find($user_id);
        // dd($data);
        return view('families.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['user'] = User::find($id);
        return view('families.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user_id)
    {
        $family = new Family;
        $family->name = $request->name;
        $family->user_id = $user_id;
        $family->nik = $request->nik;
        $family->gender = $request->gender;
        $family->date_of_birth = $request->date_of_birth;
        $family->place_of_birth = $request->place_of_birth;
        $family->save();

        return redirect('users/'.$user_id.'/family');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id,$id)
    {
        $data['user'] = User::find($user_id);
        $data['family'] = Family::find($id);

        return view('families.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$user_id, $id)
    {
        $family = Family::find($id);
        $family->name = $request->name;
        $family->nik = $request->nik;
        $family->gender = $request->gender;
        $family->date_of_birth = $request->date_of_birth;
        $family->place_of_birth = $request->place_of_birth;
        $family->save();

        return redirect('users/'.$user_id.'/family');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id,$id)
    {
        $family = Family::find($id);
        $family->delete();
    }
}
