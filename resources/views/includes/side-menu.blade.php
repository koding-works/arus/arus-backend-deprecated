<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->
        <li class="nav-header">Dashboard</li>
        <li class="nav-item">
            <a href="{{ url('dashboard')}}" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                    {{-- <span class="right badge badge-danger">New</span> --}}
                </p>
            </a>
        </li>
        <li class="nav-header">Master</li>
        <li class="nav-item">
            <a href="{{ url('counters') }}" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Counters
                    {{-- <span class="right badge badge-danger">New</span> --}}
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Doctors
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="{{ url('doctors/category') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Doctor Categories</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="{{ url('doctors') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Doctors</p>
                </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Users
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="{{ url('users') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Users</p>
                </a>
                </li>
            </ul>
        </li>
        <li class="nav-header">Auth</li>
        <li class="nav-item">
            <a href="{{ url('/logout') }}" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Logout
                    {{-- <span class="right badge badge-danger">New</span> --}}
                </p>
            </a>
        </li>

    </ul>
</nav>