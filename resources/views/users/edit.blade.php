@extends('layouts.master')
@section('title')
    <title>Users Management</title>
@endsection
@section('css')
@endsection
@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Users Management</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('content-body')
    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8 offset-md-2">
            <!-- Default box -->
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">User Update</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
                </div>
                <div class="card-body ml-2 mr-2">
                    <a href="{{ url('users') }}" class="btn btn-primary btn-sm mb-0"><i class="fas fa-arrow-left"></i> Counters List</a>
                    <hr>
                    <form action="{{ url('users/'.$user->id) }}" method="post" class="mt-2">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="inputName">Full Name</label>
                            <input name="name" type="text" placeholder="Typing doctor name..." id="inputName" class="form-control" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">User Email</label>
                            <input name="email" type="text" placeholder="Typing doctor email..." id="inputEmail" class="form-control" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label for="inputPhone">User Phone</label>
                            <input name="phone" type="text" placeholder="Typing doctor phone..." id="inputPhone" class="form-control" value="{{ $user->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="inputNik">NIK</label>
                            <input name="nik" type="number" placeholder="Typing user phone..." id="inputNik" class="form-control" value="{{$user->nik}}">
                        </div>
                        <div class="button col-3">
                            <button type="submit" class="btn btn-block bg-gradient-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
                {{-- <div class="card-footer">
                Footer
                </div> --}}
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js')
<script>
    $(document).ready(()=>{
    })
</script>
@endsection